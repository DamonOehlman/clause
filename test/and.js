var test = require('tape');
var clause = require('..');

test('combine two equality tests with an AND', function(t) {
  t.plan(1);
  t.equal(clause('test=foo&test2=bar'), '(test = "foo") AND (test2 = "bar")');
});