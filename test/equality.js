var test = require('tape');
var clause = require('..');

test('string equality', function(t) {
  t.plan(1);
  t.equal(clause('test=foo'), '(test = "foo")');
});

test('integer equality', function(t) {
  t.plan(1);
  t.equal(clause('test=5'), '(test = 5)');
});

test('numeric equality', function(t) {
  t.plan(1);
  t.equal(clause('test=5.2'), '(test = 5.2)');
});

test('numeric equality (remove insignificant decimals)', function(t) {
  t.plan(1);
  t.equal(clause('test=5.0'), '(test = 5)');
});

test('boolean equality (true)', function(t) {
  t.plan(1);
  t.equal(clause('test=true'), '(test = true)');
});

test('boolean equality (false)', function(t) {
  t.plan(1);
  t.equal(clause('test=false'), '(test = false)');
});

test('is defined', function(t) {
  t.plan(1);
  t.equal(clause('test'), '(test IS NOT NULL)');
});