var test = require('tape');
var clause = require('..');

test('deal with array values', function(t) {
  t.plan(1);
  t.equal(clause('test=foo&test=bar'), '(test IN ["foo","bar"])');
});