/* jshint node: true */
'use strict';

var SqlCondition = require('./');
var util = require('util');
var reBoolean = /^(true|false)$/i;

/**
  ### SqlFieldCondition

  ```js
  SqlFieldCondition(fieldName, comparior, value)
  ```

  The field condition type represents a simple field comparison operation,
  such as `name = "Bob"` or `age = 35`.  This type also encapsulates simple
  provides `IS NOT NULL` checks if the value is left as an empty string.

  Additionally, if the value argument of the constructor is passed an array
  then the operator will change from whatever was specified as a default to 
  an `IN` test, e.g. `age in [35, 36]`.
   
**/
function SqlFieldCondition(name, comparator, value) {
  var testFloat = parseFloat(value);

  // initialise
  this.name = name;
  this.comparator = comparator;
  this.value = isNaN(testFloat) ? value : testFloat;
}

module.exports = SqlFieldCondition;
util.inherits(SqlFieldCondition, SqlCondition);

/**
  #### toString()

  Convert a standard field condition to a string
**/
SqlFieldCondition.prototype.toString = function() {
  var comparator = Array.isArray(this.value) ? 'IN' : this.comparator;
  var customVal;

  // if the value is an empty string, convert to an IS NOT NULL clause
  if (this.value === '') {
    comparator = 'IS';
    customVal = 'NOT NULL';
  }
  // if this value is a boolean, then use a custom value
  if (reBoolean.test(this.value)) {
    customVal = this.value === 'true';
  }
  
  return '(' + [
    this.name,
    comparator,
    typeof customVal != 'undefined' ? customVal : JSON.stringify(this.value)
  ].join(' ') + ')';
};
