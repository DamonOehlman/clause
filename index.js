/* jshint node: true */
'use strict';

var util = require('util');
var sql92 = require('./dialects/sql92');

/**
  # clause

  A small helper module used to convert query string parameters into a SQL
  where clause that can be used in various SQL dialects.

  ## Reference 

**/


/**
  ### clause(input, dialect?)

  Parse the input query string based on the specified SQL dialect (defaults
  to SQL92)

**/
var clause = module.exports = function(input, dialect) {
  // if we don't have an object, then parse using the querystring module
  if (typeof input == 'string' || (input instanceof String)) {
    input = require('querystring').parse(input);
  }

  // convert to the where clause using the specified dialect
  return (typeof dialect == 'function' ? dialect : sql92)(input);
};