# clause

A small helper module used to convert query string parameters into a SQL
where clause that can be used in various SQL dialects.


[![NPM](https://nodei.co/npm/clause.png)](https://nodei.co/npm/clause/)

[![Build Status](https://travis-ci.org/DamonOehlman/clause.png?branch=master)](https://travis-ci.org/DamonOehlman/clause)
[![experimental](http://hughsk.github.io/stability-badges/dist/experimental.svg)](http://github.com/hughsk/stability-badges)

## Reference

### clause(input, dialect?)

Parse the input query string based on the specified SQL dialect (defaults
to SQL92)

## Condition Types

### SqlCondition

This is the base type for all sql conditions.

## SQL Dialect Generators

### SqlFieldCondition

```js
SqlFieldCondition(fieldName, comparior, value)
```

The field condition type represents a simple field comparison operation,
such as `name = "Bob"` or `age = 35`.  This type also encapsulates simple
provides `IS NOT NULL` checks if the value is left as an empty string.

Additionally, if the value argument of the constructor is passed an array
then the operator will change from whatever was specified as a default to 
an `IN` test, e.g. `age in [35, 36]`.

#### toString()

Convert a standard field condition to a string

### SQL92

## parse(data)

The parse function is used to extract the object querystring data into an
array of parameter instructions that can be converted into a SQL dialect
specific where clause.  The function is exposed so custom dialects can reuse
this portion of the code and implement their own mapping of the fields to
their dialect.
