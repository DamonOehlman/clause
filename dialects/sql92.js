/* jshint node: true */
'use strict';

var SqlFieldCondition = require('../condition/field');

/**
  ### SQL92

**/
module.exports = function(data) {
  return parse(data).map(stringifyCondition).filter(identity).join(' AND ');
}

/**
  ## parse(data)

  The parse function is used to extract the object querystring data into an
  array of parameter instructions that can be converted into a SQL dialect
  specific where clause.  The function is exposed so custom dialects can reuse
  this portion of the code and implement their own mapping of the fields to
  their dialect.
**/
function parse(data) {
  var key;
  var val;
  var conditions = [];

  // iterate through the opts 
  for (key in data) {
    if (data.hasOwnProperty(key)) {
      // get the value
      val = data[key];

      // depending on the type of value, convert into a sql clause
      if (typeof val == 'object' && (! (val instanceof String)) && (! Array.isArray(val))) {
      }
      else {
        conditions[conditions.length] = new SqlFieldCondition(key, '=', val);
      }
    }
  }

  return conditions;
};

function stringifyCondition(value) {
  switch (value.constructor.name) {

  default:
    return value.toString();

  }

  return '';
}

function identity(value) {
  return value;
}